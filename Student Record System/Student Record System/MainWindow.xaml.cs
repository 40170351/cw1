﻿//
// MainWindow.xaml.cs
// Student Record System
// 
// Andrew Walker
// Class for main window to allow user to enter student details
// 09/10/2015
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Student_Record_System {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow: Window {
        dynamic student = new Student(); //Changable variable for research and normal students
        bool isSet = false; //Check for award window to ensure data is available to present
        bool isValid = true; //Checks that no validation has valid

        public static int tryConversion(String number, ref bool validCheck) {
            int result;

            if (Int32.TryParse(number, out result)) {
                //Success
            } else {
                validCheck = false; 

                Console.Write("Please enter a valid value: ");
            }
            return result;
        }

        private void setButton_Click(object sender, RoutedEventArgs e) {
            //Sets and validates data entered to text boxes

            isValid = true;
            isSet = true;
            string catchString = "Fields cannot be empty";

            if (topicTextBox.Text != "" || supervisorTextBox.Text != "") {
                //Checks if student is a research student

                student = new ResearchStudent();

                try {
                    student.Supervisor = supervisorTextBox.Text;
                    catchString = "Not a valid supervisor";

                    student.Topic = topicTextBox.Text;
                    catchString = "Not a valid topic";
                } 
                catch (Exception ex) {
                    isValid = false;

                    MessageBoxResult error = MessageBox.Show(catchString);
                }
            } else {
                student = new Student();
            }

            try {
                student.FirstName = firstNameTextBox.Text;
                catchString = "Not a valid first name";

                student.SecondName = secondNameTextBox.Text;
                catchString = "Not a valid second name";

                student.DateOfBirth = dateOfBirthTextBox.Text;
                catchString = "Not a valid date of birth";

                student.MatriculationNumber = tryConversion(matricNumberTextBox.Text, ref isValid);
                catchString = "Not a valid matriculation number";

                student.Course = courseTextBox.Text;
                catchString = "Not a valid course name";

                student.Credits = tryConversion(creditsTextBox.Text, ref isValid);
                catchString = "Not a valid number of credits";

                student.Level = Int32.Parse(levelTextBox.Text);
                catchString = "Not a valid level";
            } 
            catch (Exception ex) {
                isValid = false;

                MessageBoxResult error = MessageBox.Show(catchString);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e) {
            //On clear button click

            isSet = false;
            levelTextBox.Text = "1";

            foreach (UIElement element in studentForm.Children) {
                //Loops through all UIElements on window to find text boxes

                TextBox textBox = element as TextBox;
                if (textBox != null && textBox != levelTextBox) {
                    //Sets textbox contents to empty unless textbox is level

                    textBox.Text = String.Empty;
                }
            }
        }

        private void advanceButton_Click(object sender, RoutedEventArgs e) {
            //Validates and advances student level

            if (isSet == false) {
                //Checks to ensure details have been entered

                MessageBoxResult error = MessageBox.Show("Please first set before advancing level.");
            } else {
                Console.WriteLine(student.Credits);
                student.advance();

                levelTextBox.Text = (student.Level).ToString();
            }
        }

        private void getButton_Click(object sender, RoutedEventArgs e) {
            //Retrieves data from class

            if (isSet != true) {
                MessageBoxResult error = MessageBox.Show("Please click the set button before trying to retrieve data.");
            }

            matricNumberTextBox.Text = (student.MatriculationNumber).ToString();
            firstNameTextBox.Text = student.FirstName;
            secondNameTextBox.Text = student.SecondName;
            dateOfBirthTextBox.Text = student.DateOfBirth;
            courseTextBox.Text = student.Course;
            levelTextBox.Text = (student.Level).ToString();
            creditsTextBox.Text = (student.Credits).ToString();

            if (student is ResearchStudent) {
                //Displays additional information if student is research student

                topicTextBox.Text = student.Topic;
                supervisorTextBox.Text = student.Supervisor;
            }
        }

        private void awardButton_Click(object sender, RoutedEventArgs e) {
            //On award button click

            if (isSet == true && isValid == true) {
                //Checks if details have been submitted and that all values are valid

                Award awardWindow;

                if (student is ResearchStudent) {
                    //Passes data to award window class based on student type

                    if (student.Level == 4) {
                        awardWindow = new Award(student.MatriculationNumber, student.FirstName, student.SecondName, "Phd", student.award(student.Credits, student), student.Topic, student.Supervisor);
                        awardWindow.Show();
                    } else {
                        MessageBoxResult error = MessageBox.Show("Research students must be level 4 to display an award.");
                    }
                } else {
                    awardWindow = new Award(student.MatriculationNumber, student.FirstName, student.SecondName, student.Course, student.award(student.Credits, student), null, null);
                    awardWindow.Show();
                }
            } else {
                //Displays error is data has not yet been set

                MessageBoxResult error = MessageBox.Show("Please enter valid values and click the set button before trying to display award.");
            }
        }
    }
}
