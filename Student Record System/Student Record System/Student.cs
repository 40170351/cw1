﻿//
// Student.cs
// Student Record System
// 
// Andrew Walker
// Class to store student details
// 09/10/2015
// 

using System;
using System.Windows;
public class Student {
    private int matriculationNumber; //Student's unique identification number
    private string firstName; //Student's first name
    private string secondName; //Student's second name
    private string dateOfBirth; //Student's date of birth
    private string course; //Student's current course
    private int level = 1; //Student's current year of study
    private int credits; //Student's current credit total
    private string classification; //Calculated student award

    public int MatriculationNumber {
        get {
            return matriculationNumber;
        }
        set {
            Console.WriteLine(value);

            if (value < 40000 || value > 60000) {
                //Checks that matriculation number is within the valid range of 40000 and 60000

                throw new Exception();
            }

            matriculationNumber = value;
        }
    }

    public string FirstName {
        get {
            return firstName;
        }
        set {
            if (value.Length == 0) {
                //Checks that first name field is not blank

                throw new Exception();
            }

            firstName = value;
        }
    }

    public string SecondName {
        get {
            return secondName;
        }
        set {
            if (value.Length == 0) {
                //Checks that second name field is not blank

                throw new Exception();
            }

            secondName = value;
        }
    }

    public string DateOfBirth {
        get {
            return dateOfBirth;
        }
        set {
            if (value.Length == 0) {
                //Checks that date of birth field is not blank

                throw new Exception();
            }

            dateOfBirth = value;
        }
    }

    public string Course {
        get {
            return course;
        }
        set {
            if (value.Length == 0) {
                //Checks that course field is not blank

                throw new Exception();
            }

            course = value;
        }
    }

    public int Level {
        get {
            return level;
        }
        set {
            if (value > 4) {
                //Checks if student has reached the maximum level and cannot progress futher

                throw new Exception();
            }

            if ((value == 2 && credits < 120) || (value == 3 && credits < 240) || (value == 4 && credits < 360)) {
                //Checks if student does not have enough credits to progress to next level

                throw new Exception();
            }

            level = value;
        }
    }

    public int Credits {
        get {
            return credits;
        }
        set {
            if (value < 0 || value > 480) {
                //Checks if credits are within range of 0 and 480

                throw new Exception();
            }

            credits = value;
        }
    }

    public string Classification {
        get {
            return classification;
        }
        set {
            classification = value;
        }
    }

    public void advance() {
        //Attempts to progress level by one stage if validation succeeds

        try {
            Level += 1;
        } catch (Exception exception) {
            Console.WriteLine(exception);
        }
    }

    public string award(int credits, dynamic studentType) {
        //Calculates final award for student based on mumber of credits and student type

        if (studentType.GetType() == typeof(ResearchStudent)) {
            classification = "Doctor of Philosophy";
        } else if (credits <= 359) {
            classification = "Certificate of Higher Education";
        } else if (credits >= 360 && credits <= 479) {
            classification = "Degree";
        } else {
            classification = "Honours degree";
        }

        return classification;
    }
}