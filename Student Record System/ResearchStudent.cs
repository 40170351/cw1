﻿//
// ResearchStudent.cs
// Student Record System
// 
// Andrew Walker
// Class to store research student details
// 09/10/2015
// 

using System;
using System.Windows;
public class ResearchStudent: Student {
    //Research student inherits from standard student class

    private string supervisor; //Research Student's supervisor
    private string topic; //Research Student's topic

    public string Supervisor {
        get {
            return supervisor;
        }
        set {
            if (value.Length == 0) {
                //Checks if supervisor field is not empty

                MessageBoxResult error = MessageBox.Show("Not a valid supervisor name.");
                throw new ArgumentException("Not in range");
            }

            supervisor = value;
        }
    }

    public string Topic {
        get {
            return topic;
        }
        set {
            if (value.Length == 0) {
                //Checks if topic field is not empty

                MessageBoxResult error = MessageBox.Show("Not a valid topic.");
                throw new ArgumentException("Not in range");
            }

            topic = value;
        }
    }
}