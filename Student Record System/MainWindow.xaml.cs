﻿//
// MainWindow.xaml.cs
// Student Record System
// 
// Andrew Walker
// Class for main window to allow user to enter student details
// 09/10/2015
// 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Student_Record_System {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow: Window {
        dynamic student = new Student(); //Changable variable for research and normal students
        bool isSet = false; //Check for award window to ensure data is available to present
        bool isValid = true; //Checks that no validation has valid
        string courseText = String.Empty; //Preserves text to be readded to course text box if required

        public void testHarness(int testNumber) {
            //Test data to be displayed when test buttons are clicked

            if (testNumber == 1) {
                //If test button one is clicked

                firstNameTextBox.Text = "Andrew";
                secondNameTextBox.Text = "Walker";
                dateOfBirthTextBox.Text = "22/10/1996";
                courseTextBox.Text = "Computing (BEng)";
                matricNumberTextBox.Text = "40170";
                creditsTextBox.Text = "340";
            } else if (testNumber == 2) {
                //If test button two is clicked

                firstNameTextBox.Text = "Andrew";
                secondNameTextBox.Text = "Walker";
                dateOfBirthTextBox.Text = "22/10/1996";
                courseTextBox.Text = "Computing (BEng)";
                matricNumberTextBox.Text = "70170";
                creditsTextBox.Text = "2";
            }
        }

        public static int tryConversion(String number, ref bool validCheck) {
            int result;

            if (Int32.TryParse(number, out result)) {
                //If entry can be converted to int do nothing
            } else {
                validCheck = false; 

                Console.Write("Please enter a valid value: ");
            }
            return result;
        }

        public string courseTextBoxContent(TextBox textBoxChanged) {
            //Called when either research student text box changes

            if (courseTextBox.Text != "Not required") {
                //Doesn't set unless text was entered by user

                courseText = courseTextBox.Text;
            }

            if (textBoxChanged.Text.Length == 0 && textBoxChanged.Text.Length == 0) {
                //Checks if either text box has data to check if user wants to enter research student

                courseTextBox.Text = courseText;
            } else {
                courseTextBox.Text = "Not required";
            }

            return "";
        }

        private void setButton_Click(object sender, RoutedEventArgs e) {
            //Sets and validates data entered to text boxes

            isValid = true;
            isSet = true;
            string catchString = "Fields cannot be empty";

            if (topicTextBox.Text != "" || supervisorTextBox.Text != "") {
                //Checks if student is a research student

                student = new ResearchStudent();

                try {
                    //Tries to set all attributes and sets up error to be displayed if validation fails

                    student.Supervisor = supervisorTextBox.Text;
                    catchString = "Not a valid supervisor";

                    student.Topic = topicTextBox.Text;
                    catchString = "Not a valid topic";
                } 
                catch {
                    //Diplays error in message box

                    isValid = false;
                    isSet = false;

                    MessageBoxResult error = MessageBox.Show(catchString);
                }
            } else {
                student = new Student();
            }

            try {
                //Tries to set all attributes and sets up error to be displayed if validation fails

                catchString = "Not a valid first name";
                student.FirstName = firstNameTextBox.Text;

                catchString = "Not a valid second name";
                student.SecondName = secondNameTextBox.Text;

                catchString = "Not a valid date of birth";
                student.DateOfBirth = dateOfBirthTextBox.Text;

                catchString = "Not a valid matriculation number";
                student.MatriculationNumber = tryConversion(matricNumberTextBox.Text, ref isValid);

                if (topicTextBox.Text == "" && supervisorTextBox.Text == "") {
                    //Course is not needed for research student

                    catchString = "Not a valid course name";
                    student.Course = courseTextBox.Text;
                }

                catchString = "Not a valid number of credits";
                student.Credits = tryConversion(creditsTextBox.Text, ref isValid);

                catchString = "Not a valid level";
                student.Level = Int32.Parse(levelTextBox.Text);
            } 
            catch {
                //Diplays error in message box

                isValid = false;
                isSet = false;

                MessageBoxResult error = MessageBox.Show(catchString);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e) {
            //On clear button click

            foreach (UIElement element in studentForm.Children) {
                //Loops through all UIElements on window to find text boxes

                TextBox textBox = element as TextBox;
                if (textBox != null && textBox != levelTextBox) {
                    //Sets textbox contents to empty unless textbox is level

                    textBox.Text = String.Empty;
                }
            } 
            
            levelTextBox.Text = "1";
            creditsTextBox.Text = "0";
        }

        private void advanceButton_Click(object sender, RoutedEventArgs e) {
            //Validates and advances student level

            if (isSet != true) {
                //Checks to ensure details have been entered

                MessageBoxResult error = MessageBox.Show("Please first set before advancing level.");
            } else {
                try {
                    student.advance();
                    levelTextBox.Text = (student.Level).ToString();
                } catch (Exception exception) {
                    MessageBoxResult error = MessageBox.Show(exception.Message);
                }
            }
        }

        private void getButton_Click(object sender, RoutedEventArgs e) {
            //Retrieves data from class

            if (isSet != true) {
                //Checks that data is available to get

                MessageBoxResult error = MessageBox.Show("Please click the set button before trying to retrieve data.");
            } else {
                matricNumberTextBox.Text = (student.MatriculationNumber).ToString();
                firstNameTextBox.Text = student.FirstName;
                secondNameTextBox.Text = student.SecondName;
                dateOfBirthTextBox.Text = student.DateOfBirth;
                courseTextBox.Text = student.Course;
                levelTextBox.Text = (student.Level).ToString();
                creditsTextBox.Text = (student.Credits).ToString();

                if (student is ResearchStudent) {
                    //Displays additional information if student is research student

                    topicTextBox.Text = student.Topic;
                    supervisorTextBox.Text = student.Supervisor;
                }
            }
        }

        private void awardButton_Click(object sender, RoutedEventArgs e) {
            //On award button click

            if (isSet == true && isValid == true) {
                //Checks if details have been submitted and that all values are valid

                Award awardWindow;

                if (student is ResearchStudent) {
                    //Passes data to award window class based on student type

                    if (student.Level == 4) {
                        //Checks that student is definitely level 4 as required by research students

                        awardWindow = new Award(student.MatriculationNumber, student.FirstName, student.SecondName, "Phd", student.award(student.Credits, student), student.Topic, student.Supervisor, student.Level);
                        awardWindow.Show();
                    } else {
                        MessageBoxResult error = MessageBox.Show("Research students must be level 4 to display an award.");
                    }
                } else {
                    awardWindow = new Award(student.MatriculationNumber, student.FirstName, student.SecondName, student.Course, student.award(student.Credits, student), null, null, student.Level);
                    awardWindow.Show();
                }
            } else {
                //Displays error is data has not yet been set

                MessageBoxResult error = MessageBox.Show("Please enter valid values and click the set button before trying to display award.");
            }
        }

        private void topic_TextChanged(object sender, TextChangedEventArgs e) {
            //Topic text box content changed

            courseTextBoxContent(topicTextBox);

        }

        private void supervisor_TextChanged(object sender, TextChangedEventArgs e) {
            //Supervisor text box content changed

            courseTextBoxContent(supervisorTextBox);
        }

        private void testButton1_Click(object sender, RoutedEventArgs e) {
            testHarness(1);
        }

        private void testButton2_Click(object sender, RoutedEventArgs e) {
            testHarness(2);
        }
    }
}
